import React from 'react';
import './App.css';
import './CSS-Cards/cards.css';
import Card from './Card/Card';
import { CardDeck } from './CardDeck/CardDeck';



class App extends React.Component {
  state = {
    deck: [
      { back: true },
      { back: true },
      { back: true },
      { back: true },
      { back: true }
    ],
    result: "Hand not dealt"
  }

  getOutcome = (hand) => {
    let outcome = "High Card";
    let count = 0;
    let suitCount = 0;
    let comboHand = []
    for (let i = 0; i < hand.length; i++) {
      let cardNow = hand[i]
      for (let i = 0; i < hand.length; i++) {
        if (cardNow.suit === hand[i].suit) {
          suitCount++
        }
        if (cardNow.rank === hand[i].rank && cardNow.suit === hand[i].suit) {

        } else if (cardNow.rank === hand[i].rank && cardNow.suit !== hand[i].suit) {
          count++
          comboHand.push(cardNow)
        }
      }
      console.log(cardNow)
    }
    if (suitCount === 25) {
      outcome = "flush"
    } else {
      switch (count) {
        case 2:
          outcome = "pair"
          break;
        case 4:
          outcome = "two pairs"
          break;
        case 6:
          outcome = " three of a kind"
          break;
        case 9:
          outcome = "four of a kind"
          break;
        case 8:
          outcome = "full house"
          break;

        default:
          break;
      }
    }
    console.log(count, suitCount, comboHand, outcome)
    return (outcome)
  }
  dealCards = () => {
    let deck = [...this.state.deck];
    const newDeck = new CardDeck();
    deck = newDeck.getCards(5);
    let outcome = this.getOutcome(deck);
    this.setState({ deck, result: outcome });
  }
  render() {
    return (
      <div className="App playingCards">
        <div className="Hand">
          <h1>Your hand:</h1>
          <ul className="table">
            <li><Card rank={this.state.deck[0].rank} suit={this.state.deck[0].suit} back={this.state.deck[0].back} /></li>
            <li><Card rank={this.state.deck[1].rank} suit={this.state.deck[1].suit} back={this.state.deck[1].back} /></li>
            <li><Card rank={this.state.deck[2].rank} suit={this.state.deck[2].suit} back={this.state.deck[2].back} /></li>
            <li><Card rank={this.state.deck[3].rank} suit={this.state.deck[3].suit} back={this.state.deck[3].back} /></li>
            <li><Card rank={this.state.deck[4].rank} suit={this.state.deck[4].suit} back={this.state.deck[4].back} /></li>
          </ul>
        </div>
        <div>
          <button type="button" className="Deal" onClick={this.dealCards} >Deal cards</button>
          <p>Result:  <b>{this.state.result.toLocaleUpperCase()}</b> </p>
        </div>
      </div>
    );
  };
}

export default App;
