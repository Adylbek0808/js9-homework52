import React from 'react';

const Card = props => {
    if (props.back === true) {
        return(
            <div className ="card back">*</div>
        )
    } else {
        let sym;
        switch (props.suit) {
            case 'spades':
                sym = "♠"
                break;
            case 'hearts':
                sym = "♥"
                break;
            case 'diams':
                sym = "♦"
                break;
            case 'clubs':
                sym = "♣"
                break;

            default:
                break;
        }
        return (
            <div className={"card rank-" + props.rank + " " + props.suit}>
                <span className='rank'>{props.rank.toUpperCase()}</span>
                <span className='suit'>{sym}</span>
            </div>
        );
    }
};

export default Card;