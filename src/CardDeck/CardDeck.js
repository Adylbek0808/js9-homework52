export class CardDeck {
    constructor() {
        this.deck = [];
        for (let i = 0; i < 4; i++) {
            if (i === 0) {
                const suit = "spades";
                for (let i = 0; i < 13; i++) {
                    let card;
                    if (i < 9) {
                        card = { suit: suit, rank: JSON.stringify(i + 2) };
                    } else if (i === 9) {
                        card = { suit: suit, rank: 'j' };
                    } else if (i === 10) {
                        card = { suit: suit, rank: 'q' };
                    } else if (i === 11) {
                        card = { suit: suit, rank: 'k' };
                    } else if (i === 12) {
                        card = { suit: suit, rank: 'a' };
                    }
                    this.deck.push(card);
                }
            } else if (i === 1) {
                const suit = "hearts";
                for (let i = 0; i < 13; i++) {
                    let card;
                    if (i < 9) {
                        card = { suit: suit, rank: JSON.stringify(i + 2) };
                    } else if (i === 9) {
                        card = { suit: suit, rank: 'j' };
                    } else if (i === 10) {
                        card = { suit: suit, rank: 'q' };
                    } else if (i === 11) {
                        card = { suit: suit, rank: 'k' };
                    } else if (i === 12) {
                        card = { suit: suit, rank: 'a' };
                    }
                    this.deck.push(card);
                }
            } else if (i === 2) {
                const suit = "clubs"
                for (let i = 0; i < 13; i++) {
                    let card;
                    if (i < 9) {
                        card = { suit: suit, rank: JSON.stringify(i + 2) };
                    } else if (i === 9) {
                        card = { suit: suit, rank: 'j' };
                    } else if (i === 10) {
                        card = { suit: suit, rank: 'q' };
                    } else if (i === 11) {
                        card = { suit: suit, rank: 'k' };
                    } else if (i === 12) {
                        card = { suit: suit, rank: 'a' };
                    }
                    this.deck.push(card);
                }
            } else if (i === 3) {
                const suit = "diams"
                for (let i = 0; i < 13; i++) {
                    let card;
                    if (i < 9) {
                        card = { suit: suit, rank: JSON.stringify(i + 2) };
                    } else if (i === 9) {
                        card = { suit: suit, rank: 'j' };
                    } else if (i === 10) {
                        card = { suit: suit, rank: 'q' };
                    } else if (i === 11) {
                        card = { suit: suit, rank: 'k' };
                    } else if (i === 12) {
                        card = { suit: suit, rank: 'a' };
                    }
                    this.deck.push(card);
                }
            }
        }

    }
    getCard() {
        const card = this.deck[(Math.floor(Math.random() * this.deck.length))];
        this.deck.splice(this.deck.indexOf(card), 1);
        return (card)
    }
    getCards(num) {
        const cards = [];
        for (let i = 0; i < num; i++) {
            const card = this.getCard();
            cards.push(card);
        }
        console.log(this.deck, cards)
        return (cards);
    }
}